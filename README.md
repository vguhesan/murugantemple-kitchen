This repository contains source-code for https://kitchen.murugantemple.org/

---

## Getting Started

Steps to getting started

1. Follow bitbucket instructions to clone this repository.
2. Make sure you have python (or node or equivalent) installed. Contents belong to a static site so all you will need is a server that can serve on localhost for development.
3. If you are using Python, change directory to this directory.
```
cd <this_project_folder>
```
4. Run built-in Python webserver.
```
python -m SimpleHTTPServer 8000
```
5. Open browser and navigate to
```
http://localhost:8000/
```
6. The website should come up.

---

## Make your changes

At this point, you can make all the necessary changes locally. Once you are satisfied with thge local changes, it's time to upload them to the **live** site. 

1. You can SFTP the contents of the root directory and all all of it's sub-directories to the target folder.
2. Verify and validate your changes.

More instructions coming soon.

---

